            <div class="choose">
               <?php foreach($collections as $type=>$list): ?>
                  <div><?= $str->getTitle($type) ?> <?php include "snippets/select.php"; ?></div>
               <?php endforeach ?>
            </div>
            <table>
            <tr class="first">
               <th>Title</th>
               <th>Found</th>
            </tr>
            <?php $count = 0 ?>
            <?php foreach ($playlist as $time => $item): ?>
               <?php $count ++ ?>
               <tr id="<?= $time ?>" <?php if($count == 1){ ?>class="on"<?php } ?>>
                  <td><?= $item["title"] ?></td>
                  <td><?= (isset($item["found"])) ? $parse->text($item["found"]): '' ?></td>
               </tr>
               <?php endforeach ?> 
            </table>
