                  <select type="select" id="<?= $type ?>">
                    <option value="-1" <?= (!isset($selected[$type])) ? "selected" : "" ?>>all</option>
                    <?php foreach($list as $key=>$item): ?>
                     <option value="<?= $key ?>" <?= (isset($selected[$type]) && $key == $selected[$type]) ? "selected" : "" ?> <?= (isset($enabled[$type]) && !in_array($key, $enabled[$type])) ? "disabled" : "" ?>><?= $str->format($type, $item) ?><?php if($type == "country"){ echo " ".strtoupper($item); } ?></option>
                    <?php endforeach ?>
                  </select>