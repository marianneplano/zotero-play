<?php

class Lists extends Db{

	public $enabled = [];
	public $selected = [];

	public function writeList($name, $output){

		$file = "content/".$output.".json";
		$already = (file_exists($file)) ? json_decode(file_get_contents($file), true) : false;
		$force = ["update" => true, "sc_embed" => false];
		$playlist = $this->getUniqueList($name, $already, $force);

		file_put_contents($file, json_encode($playlist));
		file_put_contents("content/tags.json", json_encode($this->allTags));

	}

	public function getList($name){

		return json_decode(file_get_contents("content/".$name.".json"), true);
	}

	public function getByID($id){

		return $this->getList("playlist")[$id];

	}

	public function getByTags($tags){

		$list = $this->getList("playlist");
		$back = [];

		foreach(json_decode($tags) as $tag){

			foreach($list as $id=>$item){

				if(isset($item["tags"]) && in_array($tag, $item["tags"]) && !in_array($item, $back)){

					$back [$id] = $item;
				}

			}

		}

		return $back;
	}

	public function getSelected($selections){

		$list = $this->getList("playlist");
		$selecteds = json_decode($selections);
		$back = [];

		foreach($list as $id=>$item){

			$shallAdd = [];

			foreach($selecteds as $type){

				foreach($type as $field => $value){

					if(intval($value) > -1){

						$tag = $this->getList($field)[$value];
						$this->selected [$field] = intval($value);

						if(isset($item[$field]) && !is_array($item[$field]) && trim($item[$field]) == $tag || isset($item[$field]) && is_array($item[$field]) && in_array(trim($tag), $item[$field])){

							$shallAdd [] = true;

						}else{

							$shallAdd [] = false;

						}	

					}else{

						$shallAdd [] = true;

					}

				}

			} 

			if(!in_array(false, $shallAdd)){

				$back [$id] = $item;
				$this->getEnabled($item);
			}
		}

		return $back;

	}

	public function selected(){

		return $this->selected;
	}


	public function enabled(){

		return $this->enabled;
	}

	public function getEnabled($item){

		$tags = ["decade", "country", "tags"];

		foreach($tags as $tag){

			if(isset($item[$tag])){

				$list = $this->getList($tag);

				if(is_array($item[$tag])){

					foreach ($item[$tag] as $sub) {

						$this->enabled[$tag][] = array_search($sub, $list);

					}

				}else{

					$this->enabled[$tag][] = array_search($item[$tag], $list);
				}

			}
		}

	}

}