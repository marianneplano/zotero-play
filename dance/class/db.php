<?php

use App\SQLiteConnection;

class Db extends Players{

	public $allTags = [];
	public $collections = ["country" => [], "decade" => [], "found" => []];
	public $playlists = ["03_music"];
	public $diglists = ["02_DIG"];

	/* DB STRUCTURE */

	public function structure($item=false){

		$structure = [

			"fields" => [

				"url" => 1,
				"date" => 14,
				"extra" => 22,
				"language" => 87,
				"title" => 110
			]
		];

		return ($item == false) ? $structure : $structure[$item];

	}


	/* DB FUNCTIONS */

	public function __construct(){

		$this->pdo = (new SQLiteConnection())->connect();
	}

	public function addToCollection($item, $field){

		if(!in_array($item, $this->collections[$field])){

			$this->collections[$field][] = $item;
		}
	}

	public function addToCollections($obj, $field){

		if(!is_array($obj->$field)){

			$this->addToCollection($obj->$field, $field);

		}else{

			foreach($obj->$field as $item){

				$this->addToCollection($item, $field);
			}

		}


	}

	public function getFromDb($id, $obj, $field, $newFieldName = false){

		$fieldId = $this->structure("fields")[$field];
		$specialStr = ["country", "decade"];

		$valueRow = $this->pdo->query('SELECT valueID from itemData WHERE itemID = "'.$id.'" AND fieldID="'.$fieldId.'";')->fetch(\PDO::FETCH_ASSOC)['valueID'];

		if($valueRow == true) {

			$field = ($newFieldName == false) ? $field : $newFieldName;
			$value = $this->pdo->query('SELECT value from itemDataValues WHERE valueID="'.$valueRow.'"')->fetch(\PDO::FETCH_ASSOC)['value'];

			if(!in_array($field, $specialStr)){

				$obj->$field = $value;

			}else{

				if($field == "country"){

					$obj->$field = $this->toTagArray($value);

				}elseif($field == "decade"){

					$obj->$field = $this->setDate($value);
				}

			}

			if(isset($this->collections[$field])){

				$this->addToCollections($obj, $field);
			}
		}	

		return $obj;
	}


	/* TYPES OF LIST */

	public function isPlaylist($name){

		return in_array($name, $this->playlists);
	}

	public function isDiglist($name){

		return in_array($name, $this->diglists);
	}


	/* FILL FIELDS */

	public function getTags($id, $object){

		$tagIDs = $this->pdo->query('SELECT tagID from itemTags WHERE itemID = "'.$id.'";');

		$object->tags = [];

		while ($tagID = $tagIDs->fetch(\PDO::FETCH_ASSOC)){

			$tags = $this->pdo->query('SELECT name FROM tags WHERE tagID = "'.$tagID['tagID'].'";');

			while($tag = $tags->fetch(\PDO::FETCH_ASSOC)){

				$object->tags[] = trim($tag['name']);

				if(!in_array(trim($tag['name']), $this->allTags)){

					$this->allTags [] = trim($tag['name']);
				}
			}

		}

		natcasesort($this->allTags);

		if(empty($object->tags)){

			unset($object->tags);
		}

		return $object;
	}


	public function getEmbedPlayer($id, $already, $obj, $force){

		if($this->hasEmbedUrl($obj->url) !== false){

			if($force["sc_embed"] == false){

				$obj->embed = $already[$id]["embed"];

			}else{

				$obj->embed = $this->writeEmbedUrl($obj->url);

			}

		}

		return $obj;
	}


	/* FILL LIST */

	public function update($id, $already, $name, $force){

		$fields = ["url", "title", "extra" => "found", "language" => "country", "date" => "decade"];
		$obj = new stdClass;

		foreach($fields as $key=>$field){

			$obj = (is_int($key)) ? $this->getFromDb($id, $obj, $field) :  $this->getFromDb($id, $obj, $key, $field); 
		}

		if($this->isPlaylist($name)){

			$obj = $this->getEmbedPlayer($id, $already, $obj, $force);
			$obj = $this->getTags($id, $obj);

			if(isset($obj->embed) && $obj->embed == false){

				echo "Invalid Soundcloud URL for track ".$obj->title."<br>";
			}
		}

		return $obj;

	}

	public function writeCollections($name){

		if($this->isPlaylist($name)){

			foreach($this->collections as $name => $collection){

				$uniques = array_unique($collection);

				if($name == "decade"){
					
					rsort($collection);

				}else{

					sort($collection);
				}

				file_put_contents("content/".$name.".json", json_encode($collection));
			}
		}
	}


	public function getUniqueList($name, $already, $force){

		$collec = $this->pdo->query('SELECT collectionID FROM collections WHERE collectionName = "'.$name.'" LIMIT 1;')->fetch(\PDO::FETCH_ASSOC)['collectionID'];
		$items = $this->pdo->query('SELECT itemID from collectionItems WHERE collectionID = "'.$collec.'";');
		$plays = [];

		while ($item = $items->fetch(\PDO::FETCH_ASSOC)){

			$id = $item['itemID'];			
			$plays [$id] = ($force["update"] == false && !isset($already[$id]) || $force["update"] == true) ? $this->update($id, $already, $name, $force) : $already[$id];
		}

		$this->writeCollections($name);

		return array_reverse($plays, TRUE);
	}

}