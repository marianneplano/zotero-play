<?php

class Players extends Strings{

	public $supported = ["youtube", "soundcloud"];

	function curl_get_content($url, $post = false, $refer = false, $usecookie = false){

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);

		if ($post === true) {
			curl_setopt($curl, CURLOPT_POST, 1);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
		}

		if ($refer === true) {
			curl_setopt($curl, CURLOPT_REFERER, $refer);
		}

		curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/6.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.7) Gecko/20050414 Firefox/1.0.3");
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_FAILONERROR, true);

		if ($usecookie) {
			curl_setopt($curl, CURLOPT_COOKIEJAR, $usecookie);
			curl_setopt($curl, CURLOPT_COOKIEFILE, $usecookie);
		}

		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

		$html = curl_exec($curl);

		if (curl_error($curl)) {
			echo 'Loi CURL : ' . (curl_error($curl));
			curl_close($curl);
			return false;
		}else{
			curl_close($curl);
			return $html;
		}
	}

	public function getSiteName($url){

		$first = (strpos($url, "www") !== false) ? strpos($url, ".") + 1 : strpos($url, "//") + 2;
		$last = strrpos($url, ".") + 1;
		$len = $last - $first - 1;

		return substr($url, $first, $len);

	}

	public function isSupported($url){

		return (in_array($this->getSiteName($url), $this->supported));
	}

	public function getYTId($url){

		$needle = "?v=";
		$pos = strpos($url, $needle) + strlen($needle);
		$len = 11;

		return substr($url, $pos, $len);

	}

	public function SCEmbed($url){

		$data = $this->curl_get_content("https://soundcloud.com/oembed?format=json&auto_play=true&url=".$url);

		return ($data !== false) ? json_decode($data)->html : $data;
	}

	public function YTEmbed($url, $isFirst){

		$auto = ($isFirst == false) ? "?autoplay=true" : "";
		$api = ($isFirst == false) ? "&enablejsapi=1&origin=http://localhost" : "?enablejsapi=1&origin=http://localhost";

		return "https://www.youtube.com/embed/".$this->getYTId($url).$auto.$api;
	}

	public function getEmbed($item, $isFirst){

		$site = $this->getSiteName($item["url"]);

		if($site == "youtube"){

			return '<iframe src="'.$this->YTEmbed($item["url"], $isFirst).'" allow="autoplay"></iframe>';

		}else{

			return $item["embed"];
		}
	}

	public function writeEmbedUrl($url){

		return $this->SCEmbed($url);

	}

	public function hasEmbedUrl($url){

		$site = $this->getSiteName($url);

		return ($site == "soundcloud");

	}

}