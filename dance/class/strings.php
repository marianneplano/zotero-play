<?php

class Strings{

	/* DATA FORMAT */

	public function toTagArray($str){

		$list = (strpos($str, ",") > -1) ? explode(",", $str) : $str;

		if(is_array($list)){

			$new = [];

			foreach($list as $item){

				$new [] = trim(strtolower($item));  
			}

			return $new;

		}else{

			return strtolower($str);

		}
	}

	/* COUNTRY FORMAT */

	public function getFlag($country){

		$flags = [

			"AT" => "🇦🇹",
			"AU" => "🇦🇺",
			"BE" => "🇧🇪",
			"BG" => "🇧🇬",
			"BO" => "🇧🇴",
			"BR" => "🇧🇷",
			"CA" => "🇨🇦",
			"CH" => "🇨🇭",
			"CI" => "🇨🇮",
			"CL" => "🇨🇱",
			"CN" => "🇨🇳",
			"CO" => "🇨🇴",
			"CU" => "🇨🇺",
			"CZ" => "🇨🇿",
			"DE" => "🇩🇪",
			"DK" => "🇩🇰",
			"DZ" => "🇩🇿",
			"EE" => "🇪🇪",
			"EG" => "🇪🇬",
			"ES" => "🇪🇸",
			"ET" => "🇪🇹",
			"FI" => "🇫🇮",
			"FR" => "🇫🇷",
			"UK" => "🇬🇧",
			"GE" => "🇬🇪",
			"GR" => "🇬🇷",
			"HR" => "🇭🇷",
			"HU" => "🇭🇺",
			"IE" => "🇮🇪",
			"ID" => "🇮🇩",
			"IL" => "🇮🇱",
			"IN" => "🇮🇳",
			"IR" => "🇮🇷",
			"IS" => "🇮🇸",
			"IT" => "🇮🇹",
			"JM" => "🇯🇲",
			"JP" => "🇯🇵",
			"KE" => "🇰🇪",
			"KH" => "🇰🇭",
			"KR" => "🇰🇷",
			"LA" => "🇱🇦",
			"LB" => "🇱🇧",
			"LT" => "🇱🇹",
			"MA" => "🇲🇦",
			"MK" => "🇲🇰",
			"ML" => "🇲🇱",
			"MX" => "🇲🇽",
			"NE" => "🇳🇪",
			"NG" => "🇳🇬",
			"NL" => "🇳🇱",
			"NO" => "🇳🇴",
			"NZ" => "🇳🇿",
			"PE" => "🇵🇪",
			"PL" => "🇵🇱",
			"PT" => "🇵🇹",
			"RO" => "🇷🇴",
			"RS" => "🇷🇸",
			"RU" => "🇷🇺",
			"RW" => "🇷🇼",
			"SD" => "🇸🇩",
			"SE" => "🇸🇪",
			"SI" => "🇸🇮",
			"SK" => "🇸🇰",
			"SN" => "🇸🇳",
			"SY" => "🇸🇾",
			"TH" => "🇹🇭",
			"TN" => "🇹🇳",
			"TR" => "🇹🇷",
			"TW" => "🇹🇼",
			"UA" => "🇺🇦",
			"US" => "🇺🇸",
			"VE" => "🇻🇪",
			"VN" => "🇻🇳",
			"XK" => "🇽🇰"
		];

		$country = strtoupper(trim($country));

		return (isset($flags[$country])) ? $flags[$country] : $country;
	}

	/* DATE FORMAT */

	public function setDate($date){

		$yo = date("Y", strtotime($date));
		return floor((intval($yo)+1)/10)*10;
	}


	public function getYears($decade){

		return substr(strval($decade), 2)."’s";
	}


	/* GENERAL FORMAT */

	public function format($type, $str){

		if($type == "decade"){

			return $this->getYears($str);

		}elseif($type == "country"){

			return $this->getFlag($str);

		}else{

			return $str;
		}
	}

	public function getTitle($type){

		if($type == "decade"){

			return "Years";

		}elseif($type == "country"){

			return "From";

		}else{

			return "Style";
		}

	}





}