<?php

include 'vendor/autoload.php';
include 'dance/class/config.php';
include 'dance/class/SQLiteConnection.php';
include 'dance/class/strings.php';
include 'dance/class/players.php';
include 'dance/class/db.php';
include 'dance/class/list.php';

$lists = new Lists;
$play = new Players;
$parse = new Parsedown();
$str = new Strings;

$playlist = $lists->getList("playlist");
$tags = $lists->getList("tags");
$collections = ["decade" => $lists->getList("decade"), "country" => $lists->getList("country"),"tags" => $lists->getList("tags")];
