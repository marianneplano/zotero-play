<?php include 'dance/start.php' ?>
<!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8">
 <title>Lockdown: 11 days</title>
 <link rel="stylesheet" type="text/css" href="assets/css/main.css">
 <meta name="description" content="💖💖💖💖💖💖💖💖💖💖💖">
 <meta name="keywords" content="dance, DANCE, brussels, BRUSSELS, playlist, marianne, marianne plano">
 <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
</head>
<body>
 <header>
  <h1>
    <div><span class="loader" data-id="dig" data-letter="d">D</span></div>
    <div><span class="loader" data-id="about" data-letter="a">a</span></div>
    <div><span>n</span></div>
    <div><span>c</span></div>
    <div><span>e</span></div>
    <div><span>!</span></div>
  </h1>
</header>
<section id="loader"></section>
<main>
  <section id="right">
    <div id="list">
     <?php include 'snippets/playlist.php'; ?>
    </div>
 </section>
 <section id="left">
  <div id="player">
   <?= $play->getEmbed(reset($playlist), true) ?>
 </div>
</section>
</main>
<script type="text/javascript" src="assets/js/main.js"></script>
</body>
</html>