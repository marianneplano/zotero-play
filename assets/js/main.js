function loadContent(clicked, wrapper, destination, msg){

	var xhttp = new XMLHttpRequest();

	xhttp.onreadystatechange = function(){

		if (this.readyState == 4 && this.status == 200){

			wrapper.innerHTML = this.responseText;

			thenPlayer(destination, clicked);
			thenSelect(destination, clicked);
			thenLoaders(wrapper, clicked);
		}
	};
	xhttp.open("POST", destination, true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send(msg);
}

function thenLoaders(wrapper, clicked){

	if(wrapper.id == "loader"){

		wrapper.classList.add(clicked.dataset.id);
		wrapper.classList.add("on");

		var close = document.getElementById("close");

		close.addEventListener("click", function(){

			wrapper.innerHTML="";
			wrapper.classList="";
			document.body.classList.remove("hasLoader");

		});
	}
}

function onIt(clicked){

	if(!clicked.classList.contains("on")){

		clicked.classList.add("on");

	}else{

		clicked.classList.remove("on");
	}
}

function thenSelect(destination, clicked){

	if(destination == "select.php"){

		loadSongs();
		loadPlayer(document.querySelector("tr.on"), document.querySelector("tr.on").id, 1);
		tagSystem();

	}
}

function thenPlayer(destination, clicked){

	if(destination == "player.php"){

		document.querySelector("tr.on").classList.remove("on");
		onIt(clicked);

	}

}

function loadPlayer(clicked, id, first){

	var player = document.getElementById("player");

	loadContent(clicked, player, "player.php", "id="+id+"&first="+first);
}

function loadSongs(){

	var plays = document.querySelectorAll("tr:not(.first)");

	for(i=0; i<plays.length; i++){

		plays[i].addEventListener("click", function(){

			loadPlayer(this, this.id, 0);


		});
	}
}


function tagSystem(){

	var selects = document.querySelectorAll("select");

	for(i=0; i<selects.length; i++){

		selects[i].addEventListener("change", function(){

			var list = document.getElementById("list");
			var ids = [];

			for(j=0; j<selects.length; j++){

				var json = {};

				json[selects[j].id] = selects[j].value;
				ids.push(json);
			}

			loadContent(this, list, "select.php", "selected="+JSON.stringify(ids));

		});

	}
}

function loaders(){

	var loaders = document.querySelectorAll(".loader");

	for(i=0; i<loaders.length; i++){

		loaders[i].addEventListener("mouseenter", function(){

			this.innerHTML=this.dataset.id;

		});

		loaders[i].addEventListener("mouseleave", function(){

			this.innerHTML=this.dataset.letter;

		});

		loaders[i].addEventListener("click", function(){

			loadContent(this, document.getElementById("loader"), this.dataset.id+".php");
			document.body.classList.add("hasLoader");

		});
	}
}

function start(){

	loadSongs();
	tagSystem();
	loaders();
}

document.addEventListener("DOMContentLoaded", start);